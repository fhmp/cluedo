package net.fhmp;

import java.awt.Graphics2D;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.util.*;
import net.fhmp.cludos.components.*;
import net.miginfocom.swing.MigLayout;

/**
 * The model for the game board.
 *
 * @author Rudi Theunissen
 */
public class Board extends CustomComponent {

    /**
     * Starting LOCATIONS of each respective suspect token
     */
    private HashMap<String, Location> startingLocations = new HashMap<String, Location>();
    /**
     * Amount of rows on the board
     */
    public static final int ROWS = 29;
    /**
     * Amount of columns on the board
     */
    public static final int COLS = 24;
    
    
    private LinkedList<Location> highlightedLocations = new LinkedList<>();
    
    /**
     * Grid of location squares on the game board
     */
    private final Location[][] LOCATIONS = new Location[ROWS][COLS];

    private BufferedImage background = ResourceLoader.openImage("images/board.jpg");

    public void paintContent(Graphics2D g) {
        g.drawImage(background, 0, 0, getHeight(), getHeight(), null);
    }

    public Board() {

        for (int row = 0; row < ROWS; row++)
            for (int col = 0; col < COLS; col++)
                this.setLayout(new MigLayout(
                        "ins 1.25% 1.25% 1.25% 18.925%, wrap " + COLS + ", gap 1", "grow", "grow"));

        for (int row = 0; row < ROWS; row++)
            for (int col = 0; col < COLS; col++) {
                LOCATIONS[row][col] = new Location(row, col);

                this.add(LOCATIONS[row][col], "grow");
            }

        loadLocationTypes();
        setNeighbours();
    }

    private void loadLocationTypes() {
        Iterator<String> lines = ResourceLoader.readLines("locations.info").iterator();
        String line;
        while (lines.hasNext()) {
            LocationType type;
            switch (lines.next()) {
                case ":walkables":
                    type = LocationType.WALKABLE;
                    break;
                case ":intrigues":
                    type = LocationType.INTRIGUE;
                    break;
                case ":doorways":
                    type = LocationType.DOORWAY;
                    break;
                default:
                    continue;
            }
            while (!(line = lines.next()).equals(".")) {
                for (String pair : line.trim().split(" ")) {
                    parsePair(pair).setLocationType(type);
                }
            }

        }

        addEntrances(Room.SPA, 5, 5);
        addEntrances(Room.THEATRE, 7, 10, 2, 12);
        addEntrances(Room.LIVING_ROOM, 5, 14, 8, 16);
        addEntrances(Room.OBSERVATORY, 8, 22);
        addEntrances(Room.PATIO, 11, 5, 12, 7, 16, 7, 17, 5);
        addEntrances(Room.SWIMMING_POOL, 11, 14, 16, 10, 16, 17);
        addEntrances(Room.HALL, 11, 21, 11, 22, 13, 19, 14, 19);
        addEntrances(Room.KITCHEN, 22, 6);
        addEntrances(Room.DINING_ROOM, 19, 12, 21, 15);
        addEntrances(Room.GUEST_HOUSE, 20, 21, 21, 20);

        setStartingLocation(Suspect.SCARLET, 28, 18);
        setStartingLocation(Suspect.MUSTARD, 28, 7);
        setStartingLocation(Suspect.WHITE, 19, 0);
        setStartingLocation(Suspect.GREEN, 9, 0);
        setStartingLocation(Suspect.PEACOCK, 0, 6);
        setStartingLocation(Suspect.PLUM, 0, 20);
    }
    
    private void setStartingLocation(Suspect suspect, int row, int col){
        suspect.setStartingLocation(LOCATIONS[row][col]);
        LOCATIONS[row][col].setLocationType(LocationType.STARTING);
    }
    
        private void addEntrances(Room room, int ... pairs){
        for(int i = 0; i < pairs.length; i += 2){
            addEntrance(room, pairs[i], pairs[i + 1]);
        }
    }
   
    private void addEntrance(Room room, int row, int col) {
        Location loc = LOCATIONS[row][col];
        loc.setLocationType(LocationType.ENTRANCE);
        room.getEntrances().add(loc);
    }

    private Location parsePair(String pair) {
        String[] s = pair.trim().split(":");
        int row = Integer.parseInt(s[0]);
        int col = Integer.parseInt(s[1]);
        return LOCATIONS[row][col];
    }

    /**
     * Sets each grid location's valid neighbouring LOCATIONS
     */
    private void setNeighbours() {
        for (int row = 0; row < ROWS; row++)
            for (int col = 0; col < COLS; col++) {
                Location current = LOCATIONS[row][col];
                if (LOCATIONS[row][col].isBlocked()) continue;

                if (row < ROWS - 1)
                    setNeighbour(current, row + 1, col);
                if (row > 0)
                    setNeighbour(current, row - 1, col);
                if (col < COLS - 1)
                    setNeighbour(current, row, col + 1);
                if (col > 0)
                    setNeighbour(current, row, col - 1);
            }
    }

    /**
     *
     * Checks if a location is valid, then adds it to the current node's neighbours
     *
     * @param loc current node
     * @param row row of the potential neighbour
     * @param col col of the potential neighbour
     */
    private void setNeighbour(Location loc, int row, int col) {
        Location n = LOCATIONS[row][col];
        if (n == null) n = new Location(row, col);
        if (isAccessible(n, loc)) loc.addNeighbour(n);
    }

    /**
     * Prints an ASCII representation of the board. Note however that this is used mostly for debugging.
     */
    public void printBoard() {
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {
                Location loc = LOCATIONS[row][col];
                switch(loc.getLocationType()){
                    case ENTRANCE:
                        System.out.print("@");
                        break;
                    case STARTING:
                        System.out.print("O");
                        break;
                    case INTRIGUE:
                        System.out.print("?");
                        break;
                    case DOORWAY:
                        System.out.print("*");
                        break;
                    case WALKABLE:
                        System.out.print(" ");
                        break;
                    default:
                        System.out.print("#"); 
                }
            }
            System.out.println();
        }
    }

    /**
     * Returns the resulting location after moving a set amount of paces towards a given destination location.
     *
     * @param source source Location
     * @param dest destination Location
     * @param paces maximum amount of paces allows to move
     * @return the resulting location
     */
    public Location moveTowards(Location source, Location dest, int paces) {
        if (source.equals(dest) || paces == 0) return source;
        LinkedList<Location> shortestPath = shortestPath(source, dest);
        if (paces >= shortestPath.size()) return dest;
        else return shortestPath.get(paces);
    }
    
    public List<Location> pathBetween(Location source, Location dest, int paces){
       return shortestPath(source, dest).subList(0, paces);
    }

    /**
     * Returns a list of location nodes that follow the shortest path from start to destination.
     *
     * An implementation of the A* algorithm using Manhattan distance for the heuristic priority.
     *
     * @param start start location
     * @param dest target destination
     * @return
     */
    public LinkedList<Location> shortestPath(final Location start, final Location dest) {
        HashSet<Location> visits = new HashSet<>();
        HashMap<Location, Location> links = new HashMap<>();
        PriorityQueue<Location> queue = new ManhattanHeuristicQueue(dest, this);

        queue.add(start);
        while (!queue.isEmpty()) {
            Location current = queue.remove();
            if (current.equals(dest)) {
                LinkedList<Location> path = reconstruct(current, new LinkedList<Location>(), links);
                path.add(dest);
                return path;
            }

            visits.add(current);
            for (Location neighbour : current.getNeighbours()) {
                if (!visits.contains(neighbour)) {
                    queue.add(neighbour);
                    visits.add(neighbour);
                    links.put(neighbour, current);
                }
            }
        }
        return null;
    }

    /**
     * Recursively reconstructs a map of location links to generate a list of location nodes.
     *
     * @param current the current node
     * @param list the accumulating list
     * @param links the map of links
     * @return a list of nodes that follow the shortest path
     */
    private LinkedList<Location> reconstruct(Location current, LinkedList<Location> list, HashMap<Location, Location> links) {
        if (links.containsKey(current)) {
            list.addFirst(links.get(current));
            return reconstruct(links.get(current), list, links);
        } else {
            return list;
        }
    }

    /**
     * Grid LOCATIONS accessor
     *
     * @return grid LOCATIONS
     */
    public Location[][] getLocations() {
        return LOCATIONS;
    }

    /**
     * Determines if one location is legally accessible from another
     *
     * @param to towards this location
     * @param from coming from this location
     * @return true is to is a valid path via from
     */
    public boolean isAccessible(Location to, Location from) {
        if (to.isBlocked()) return false;

        if (from.isEntrance()) {
            return (to.isDoorway());
        } else if (to.isEntrance()) {
            return (from.isDoorway());
        }
        return true;
    }
    
    public void clearHighlights(){
        for(Location l : highlightedLocations){
            l.setHighlighted(false);
        }
        highlightedLocations.clear();
        repaint();
    }
    
    public void highlightReachableLocations(Location location, int paces) {
        HashSet<Location> visits = new HashSet<>();
        HashMap<Location, Integer> distances = new HashMap<>();
        distances.put(location, 0);
        
        Queue<Location> queue = new LinkedList<>();
        queue.add(location);
        while (!queue.isEmpty()) {
            Location current = queue.remove();
            visits.add(current);

            if (distances.containsKey(current) && distances.get(current) >= paces) continue;
            for (Location neighbour : current.getNeighbours()) {
                if (!visits.contains(neighbour)) {
                    queue.add(neighbour);
                    visits.add(neighbour);
                    distances.put(neighbour, distances.get(current) + 1);
                }
            }
        }
        highlightedLocations.addAll(distances.keySet());
        highlightedLocations.remove(location);
        for (Location loc : highlightedLocations) {
            loc.setHighlighted(true);
        }

        repaint();
    }

    /**
     * Fills a map with distances to key LOCATIONS on the map relative to the specified player.
     *
     * @param player the relative player
     * @param results the supplied map to set all key LOCATIONS and their distances to
     */
    public void getDistancesFromPlayer(Player player, LinkedHashMap<String, DistanceNode> results) {
        Location loc = player.getLocation();
        if (loc.isEntrance()) {
            getDistancesFromRoom(loc.getEntrance(), results);
        } else getDistances(loc, results);


        if (loc.getEntrance() != null)
            if (loc.isEntrance() && loc.getEntrance().hasSecretPassage()) {
                String passage = loc.getEntrance().getSecretPassage().toString();
                DistanceNode n = results.get(passage);
                results.put(passage, new DistanceNode(n.getSource(), n.getDestination(), -1));
            }
    }

    /**
     * Determines the distances to key LOCATIONS on the map relative to the specified room. It should be noted that the door a player used to enter the room does not necessarily have to also be the
     * door that the player has to exit with.
     *
     * @param room the room that the player is currently in
     * @param results mappings between key LOCATIONS and their distances from the specified room.
     */
    private void getDistancesFromRoom(Room room, LinkedHashMap<String, DistanceNode> results) {
        if (room.getEntrances().size() == 1) getDistances(room.getEntrances().get(0), results);
        else for (Location location : room.getEntrances()) {
                LinkedHashMap<String, DistanceNode> distances = new LinkedHashMap<String, DistanceNode>();
                getDistances(location, distances);
                for (String key : distances.keySet()) {
                    if (!results.containsKey(key)) {
                        results.put(key, distances.get(key));
                    } else {
                        if (distances.get(key).getDistance() < results.get(key).getDistance()) {
                            results.put(key, distances.get(key));
                        }
                    }
                }
            }

    }

    /**
     * Breadth-first-search to determine the distance to each key component on the board from a specified starting point.
     *
     * @param start starting location
     * @param results map that will contain the links between LOCATIONS and their distances.
     */
    private void getDistances(Location start, LinkedHashMap<String, DistanceNode> results) {
//        if (!start.isEntrance() && !start.isIntrigue())
//            results.put(Strings.get("stay_at_location"), new DistanceNode(start, start, 0));

        HashSet<Location> visits = new HashSet<Location>();
        HashMap<Location, Integer> distances = new HashMap<Location, Integer>();
        distances.put(start, 0);

        Queue<Location> queue = new LinkedList<Location>();
        queue.add(start);
        while (!queue.isEmpty()) {
            Location current = queue.remove();

            if (current.isIntrigue() && !results.containsKey("Intrigue") && !current.equals(start))
                results.put("Intrigue", new DistanceNode(start, current, distances.get(current)));
            else if (current.isEntrance() && !results.containsKey(current.getEntrance().toString()))
                if (!start.isEntrance() || !start.getEntrance().equals(current.getEntrance()))
                    results.put(current.getEntrance().toString(), new DistanceNode(start, current, distances.get(current)));

            for (Location neighbour : current.getNeighbours()) {
                if (!visits.contains(neighbour)) {
                    queue.add(neighbour);
                    visits.add(neighbour);
                    distances.put(neighbour, distances.get(current) + 1);
                }
            }
        }
    }

    public int getRows() {
        return ROWS;
    }

    public int getCols() {
        return COLS;
    }

    public Location getStartingLocation(String suspect) {
        return startingLocations.get(suspect);
    }

    public void addStartingLocation(String name, Location location) {
        startingLocations.put(name, LOCATIONS[location.row][location.col]);
    }

    public boolean isAccusationRoom(String name) {
        return name.contains("*");
    }

    /**
     * Priority queue that uses Manhattan distance to determine the heuristic search priority.
     *
     * @author PARANOID
     */
    private class ManhattanHeuristicQueue extends PriorityQueue<Location> {

        public ManhattanHeuristicQueue(final Location dest, Board board) {
            super(board.getRows() * board.getCols(), new Comparator<Location>() {

                @Override
                public int compare(Location a, Location b) {
                    return Integer.compare(getManhattanDistance(a, dest), getManhattanDistance(b, dest));
                }

                private int getManhattanDistance(Location src, Location dest) {
                    return Math.abs(dest.row - src.row) + Math.abs(dest.col - src.col);
                }
            });
        }
    }
}
