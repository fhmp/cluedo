/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.fhmp;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;


/**
 *
 * @author Paranoid
 */
public class CustomTooltip extends JPopupMenu{

    private JComponent parent;
    int mouseX = 0;
    private boolean disabled;
    public static final int TOP = 0;
    public static final int BOTTOM = 1;
    public static final int RIGHT = 2;
    public static final int LEFT = 3;
    private int location;
    private Timer hoverTime;
    private Window parentWindow;
    private String text;
    public static boolean alive;
    private static final int HOVER_TIMEOUT = 1000;
    private MouseListener hoverListener;
    private int offsetX, offsetY;

    public String getText() {
        return text;
    }
    
    
    @Override
    public void setToolTipText(String text) {
        if (text == null) return;
        this.text = text;
        FontMetrics fm = getFontMetrics(Fonts.MEDIUM_BOLD);
        int w = fm.stringWidth(text);
        this.setPreferredSize(new Dimension(w + 30, 36));
        this.setSize(w + 30, 36);
    }

    public void setAlwaysHide(boolean disabled) {
        this.disabled = disabled;
    }
    
    public Container getParentWindow(){
        return null;
    }

    public CustomTooltip(String text, JComponent parent) {
        if (parent == null) throw new IllegalArgumentException("Parent can't be null.");
        this.parent = parent;
        
        
       
        parentWindow = MainFrame.frame;
         this.setOpaque(false);
        this.setFocusable(false);
        this.setBorder(new EmptyBorder(24, 10, 10, 10));
       
        hoverTime = new Timer(HOVER_TIMEOUT, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                show();
                hoverTime.stop();
            }
        });


        hoverListener = new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                setVisible(false);
                hoverTime.stop();

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if (!disabled){
                    hoverTime.start();
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setVisible(false);
                hoverTime.stop();
            }
        };

        this.parent.addMouseListener(hoverListener);

        setToolTipText(text);
        setLocation(BOTTOM);
    }

    public final void setLocation(int location) {
        this.location = location;
    }

    @Override
    public void paintComponent(Graphics graphics) {
        this.setForeground(Colors.DARK_GRAY);
        Graphics2D g = (Graphics2D) graphics.create();
        GraphicsUtils.enableMaximumQuality(g);
        int tipHeight = 6;
        int halfTipWidth = 6;
        
        int h = getHeight();

        if (location == BOTTOM) {
            int tipCenter = parent.getLocationOnScreen().x - this.getLocationOnScreen().x + parent.getWidth() / 2;
            
            GeneralPath p = new GeneralPath();

            p.moveTo(tipCenter - halfTipWidth, tipHeight);
            p.lineTo(tipCenter, 0);
            p.lineTo(tipCenter + 1, 0);
            p.lineTo(tipCenter +  1 + halfTipWidth, tipHeight);
            p.closePath();
            

           
            Area a = new Area(new RoundRectangle2D.Float(1, tipHeight, getWidth() - 2, getHeight() - tipHeight, 4, 4));
            g.setPaint(new GradientPaint(0, 0, Colors.LIGHT_GRAY_LIGHTER, 0, getHeight(), Colors.LIGHT_GRAY_DARKER));
            if (tipCenter == getWidth() - halfTipWidth - 2)
                a.add(new Area(new Rectangle2D.Float(getWidth() - 6, tipHeight, 5, 5)));
            a.add(new Area(p));
            g.fill(a);
            
            Area area = new Area( new RoundRectangle2D.Float(1, tipHeight, getWidth() - 3, getHeight() - tipHeight - 1, 4, 4));
            area.add(new Area(p));
            g.setColor(Colors.TOOLTIP_BORDER);
            g.draw(area);

            


        } else if (location == RIGHT) {
            h -= 6;
            GeneralPath p = new GeneralPath();
          
            
            p.moveTo(0, 15);
            p.lineTo(tipHeight, 9);
            p.lineTo(tipHeight, 21);
            p.closePath();

            RoundRectangle2D rect2 = new RoundRectangle2D.Float(tipHeight, 0, getWidth() - tipHeight - 2, h, 4, 4);

            g.setPaint(new GradientPaint(0, 0, Colors.LIGHT_GRAY_LIGHTER, 0, getHeight(), Colors.LIGHT_GRAY_DARKER));
            g.fill(rect2);
            g.fill(p);
           
            

            g.setColor(Colors.TOOLTIP_BORDER);
            Area area = new Area(new RoundRectangle2D.Float(tipHeight, 0, getWidth() - tipHeight - 2, h - 1, 4, 4));
            area.add(new Area(p));
           g.draw(area);


        } 
        
         else if (location == LEFT) {
            GeneralPath p = new GeneralPath();
          
            
            p.moveTo(getWidth(), 15);
            p.lineTo(getWidth() - tipHeight, 9);
            p.lineTo(getWidth() - tipHeight, 21);
            p.closePath();

            RoundRectangle2D rect2 = new RoundRectangle2D.Float(tipHeight, 0, getWidth() - tipHeight - 2, getHeight(), 4, 4);

            g.setPaint(new GradientPaint(0, 0, Colors.LIGHT_GRAY_LIGHTER, 0, getHeight(), Colors.LIGHT_GRAY_DARKER));
            g.fill(rect2);
            g.fill(p);
           
            

            g.setColor(Colors.TOOLTIP_BORDER);
            Area area = new Area(new RoundRectangle2D.Float(tipHeight, 0, getWidth() - tipHeight - 2, getHeight() - 1, 4, 4));
            area.add(new Area(p));
           g.draw(area);


        }else if (location == TOP) {
            System.err.println("TOP Tooltip not implemented yet.");
        }
        
        if(text == null) return;
        
        
        g.setColor(Colors.DARK_GRAY);
        g.setFont(Fonts.MEDIUM_BOLD);
        int sx = 15;
        if(location == RIGHT) sx += 6;
        g.drawString(text, sx, h - 11);
    }
    
    public void setParentWindow(Window parentWindow){
        this.parentWindow = parentWindow;
    }
    
    public void setOffset(int x, int y){
        this.offsetX = x;
        this.offsetY = y;
    }

    @Override
    public void show() {
       if(text == null) return;
        try {
            int x = 0, y;
            if (this.location == TOP) y = -getHeight();
            else if (this.location == BOTTOM) {
                if(parentWindow != null && parent.getX() + getWidth() > parentWindow.getWidth()) x = -((parent.getX() + getWidth()) - parentWindow.getWidth()) - 5;
                y = parent.getHeight();
            } else y = 0;

            if (this.location == RIGHT){
                x = parent.getWidth() + 10;
                y = (parent.getHeight() - 30) / 2;
            } else if (this.location == LEFT){
                x =  - 10 - getWidth();
                y = (parent.getHeight() - 30) / 2;
            }
            
            y += offsetY;
             x += offsetX;

            super.show(this.parent, x, y);
        } catch (Exception e) {
            this.setVisible(false);
        }
    }

    @Override
    public void show(Component parent, int x, int y) {
        show();
    }

    public void setParent(JComponent parent) {
        this.parent.removeMouseListener(hoverListener);
        this.parent = parent;
        this.parent.addMouseListener(hoverListener);
    }
}