package net.fhmp;

import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.Random;
import javax.swing.JButton;
import net.fhmp.cludos.components.CustomComponent;

/**
 *
 * @author Rudi Theunissen
 */
public class MainMenu extends CustomComponent{
    
    BufferedImage background = ResourceLoader.openImage("images/background_" + (new Random().nextInt(2)+1) + ".jpg");
    
    JButton hotSeat = new JButton("Hot Seat");
    
    public MainMenu(){
        this.add(hotSeat);
        
        
        ActionListener al = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() == hotSeat){
                    MainFrame.switchTo(Views.HOT_SEAT);
                }
            }
            
        };
        
        hotSeat.addActionListener(al);
        
    }
        public void paintContent(Graphics2D g){
            double x, y, w, h;
            double ratio = (double)getWidth() / getHeight();
            if(ratio >= 1.6){
                // width of frame is greater than of window -> fit image to width of frame
                w = getWidth();
                h = getWidth() / 1.6;
                x = 0;
                y = -(h - getHeight()) / 2;
            } else {
                h = getHeight();
                w = getHeight() * 1.6;
                x = -(w - getWidth()) / 2;
                y = 0;
            }
            g.drawImage(background, (int)x, (int)y, (int)w, (int)h, null);
        }
        
       
    
    

}
