package net.fhmp;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import net.fhmp.cludos.components.CustomComponent;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Rudi Theunissen
 */
public class MainFrame{

    static void switchTo(Views view) {
        views.switchTo(view);
    }
    
    static JFrame frame = new JFrame("Cluedo");

     static   ViewSwitcher<CustomComponent> views = new ViewSwitcher<>();
     static   Model model = new Model();
     static   Controller controller = new Controller();
    

     static   GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
    

    
     static   Board boardPanel = model.getBoard();
     static   SidePanel sidePanel = new SidePanel();
     static CustomComponent hotSeat = new CustomComponent();
     JComponent content ;
//    JComponent content = new JComponent() {
//
//        TexturePaint table = new TexturePaint(ImageLoader.readImage("table.jpg"), new Rectangle(0, 0, 400, 400));
//
//        @Override  Something 'Some other thing.'; you carred
//        public void paintComponent(Graphics graphics) {
//            Graphics2D g = (Graphics2D) graphics;
//            GraphicsUtils.enableMaximumQuality(g);
//            g.setPaint(table);
//            g.fillRect(0, 0, getWidth(), getHeight());
//            super.paintComponent(g);
//        }
//    };

    public MainFrame() {
         

        content = (JComponent) frame.getContentPane();
        content.setLayout(new MigLayout());
        
        views.setHost(content);

        content.addComponentListener(new ComponentAdapter() {

            public void componentResized(ComponentEvent e) {
            }
        });

        final Menu menu = new Menu();

        MainMenu mainMenu = new MainMenu();

        frame.addMouseMotionListener(new MouseAdapter() {

            public void mouseMoved(MouseEvent e) {
                boolean hasSelectedMenu = false;
                for (int i = 0; i < menu.getMenuCount(); ++i) {
                    //     if(menu.getMenu(i) == null) continue;
                    if (menu.getMenu(i).isSelected()) {
                        hasSelectedMenu = true;
                        break;
                    }
                }

            //    if (!hasSelectedMenu)
              //      menu.setVisible(e.getY() < 25);
            }
        });


        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                exit();
            }
        });


        frame.setJMenuBar(new Menu());

        hotSeat.add(boardPanel, "id board, pos 0 0 container.h container.h");
        hotSeat.add(sidePanel, "id side, pos board.x2 0 container.w container.h");

        views.registerView(hotSeat, Views.HOT_SEAT);
        views.registerView(mainMenu, Views.MAIN_MENU);

        views.switchTo(Views.MAIN_MENU);

        frame.setUndecorated(true);
        frame.setResizable(false);



        setResolution(getMaximumResolution());
    }
    

    private static void exit() {
        System.exit(0);
    }

    private void setResolution(DisplayMode displayMode) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                //  this.dispose();
                //   device.setFullScreenWindow(this);
                //   device.setDisplayMode(displayMode);
                //   updateComponentRestraints(displayMode);      


                frame.setSize(new Dimension(1366, 768));
                frame.setVisible(true);
            }
        });

    }


    
    private DisplayMode getMaximumResolution(){
        DisplayMode [] modes = device.getDisplayModes();
        return modes[modes.length - 1];
    }


    private static class Menu extends JMenuBar {

        private enum Action {

            NEW_GAME, EXIT
        }
        ActionListener menuItemListener = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                onClick(((MenuItem) e.getSource()).action);
            }
        };

        private void onClick(Action action) {
            switch (action) {
                case NEW_GAME:  break;
                case EXIT: exit();
            }
        }

        private JMenu addMenu(String name, int mnemonic) {
            JMenu menu = new JMenu(name);
            menu.setMnemonic(mnemonic);
            this.add(menu);
            return menu;
        }

        public Menu() {

            JMenu fileMenu = addMenu("File", KeyEvent.VK_F);
            fileMenu.add(new MenuItem("New Game", Action.NEW_GAME, KeyEvent.VK_N));
            fileMenu.add(new MenuItem("Exit", Action.EXIT, KeyEvent.VK_E));
            
        //    JMenu viewMenu = createMenu("View", KeyEvent.VK_V);
            
        //    this.add(new JButton("Close"));
           

        }

        private class MenuItem extends JMenuItem {

            public final Action action;

            public MenuItem(String name, Action action, int key) {
                super(name, key);
                this.setAccelerator(KeyStroke.getKeyStroke(key, ActionEvent.CTRL_MASK));
                this.action = action;
                this.addActionListener(menuItemListener);
            }
        }
    }
}
