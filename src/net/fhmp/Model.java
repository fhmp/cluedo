package net.fhmp;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.List;
import net.fhmp.cludos.components.Room;
import net.fhmp.cludos.components.Suspect;
import net.fhmp.cludos.components.Weapon;

/**
 *
 * @author PARANOID
 */
public class Model {
 
    
    private CardSet cardSet;
    private Board board;
    
    public Model(){
        board = new Board();
        board.printBoard();
        cardSet = new CardSet();
    }
   

    public CardSet getCardSet() {
        return cardSet;
    }

    public Board getBoard() {
        return board;
    }
}