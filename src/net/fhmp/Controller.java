package net.fhmp;

import java.util.ArrayList;
import net.fhmp.cludos.components.Player;

/**
 *
 * @author Rudi Theunissen
 */
class Controller {

    private ArrayList<Player> players;
    private Player currentPlayer;

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public void start() {
        endTurn();
    }
    
    public void endTurn(){
        currentPlayer = players.remove(0);
        MainFrame.sidePanel.setIndicatorPlayer(currentPlayer);
        players.add(currentPlayer);
        
        
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void movePlayer(Location to) {
        currentPlayer.setLocation(to);
        MainFrame.boardPanel.clearHighlights();
        onMove();
        
    }
    
    private void onMove(){
        switch(currentPlayer.getLocation().getLocationType()){
            case ENTRANCE:
                // landed on entrance
                break;
            case INTRIGUE:
                // draw intrigue card
                break;
            default:
        }
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }
}
