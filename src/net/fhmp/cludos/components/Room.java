
package net.fhmp.cludos.components;

import java.awt.geom.Area;
import java.util.ArrayList;
import java.util.List;
import net.fhmp.Location;

/**
 * Represents a room card.
 * 
 * @author Rudi Theunissen
 */
public enum Room implements Card{
    
    SPA("Spa"),
    THEATRE("Theatre"),
    LIVING_ROOM("Living Room"),
    OBSERVATORY("Observatory"),
    PATIO("Patio"),
    SWIMMING_POOL("Swimming Pool"),
    HALL("Hall"),
    KITCHEN("Kitchen"),
    DINING_ROOM("Dining Room"),
    GUEST_HOUSE("Guest House");

    static{
        setSecretPassage(KITCHEN, OBSERVATORY);
        setSecretPassage(GUEST_HOUSE, SPA);
    }
    
    private Room(String name){
        this.name = name;
    }
    
    private Room secretPassage;
    private String name;
    
    private final ArrayList<Location> entrances = new ArrayList<>(3);
    
    @Override
    public String toString(){
        return name;
    }
    
    private static void setSecretPassage(Room a, Room b){
        a.secretPassage = b;
        b.secretPassage = a;
    }
    
    public Room getSecretPassage(){
        return secretPassage;
    }
    
    public boolean hasSecretPassage(){
        return secretPassage != null;
    }
   

    public List<Location> getEntrances() {
        return entrances;
    }
}
