
package net.fhmp.cludos.components;

/**
 * Simple enum to keep track of the different types of cards.
 * 
 * @author PARANOID
 */
public enum CardType {

    /** All possible card types in the game */
    WEAPON, ROOM, SUSPECT, KEEPER, CLOCK;
}