
package net.fhmp.cludos.components;

import net.fhmp.Location;

/**
 *
 * @author Rudi Theunissen
 */
public class DistanceNode {

    private final Location from, to;
    private final int distance;

    public DistanceNode(Location from, Location to, int distance) {
        this.from = from;
        this.to = to;
        this.distance = distance;
    }

    public int getDistance() {
        return distance;
    }
    
    public Location getDestination(){
        return to;
    }

    public Location getSource() {
        return from;
    }
    
    @Override
    public String toString(){
        return from + " -> " + to + " = " + distance;
    }
}
