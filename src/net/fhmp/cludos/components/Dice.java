package net.fhmp.cludos.components;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.fhmp.Colors;
import net.fhmp.ResourceLoader;
import net.fhmp.SVGComponent;

public class Dice extends CustomComponent {
    
    private BufferedImage question = ResourceLoader.openImage("images/question_mark.png");

    private Die left; // component for one die
    private Die right;
    private int count;
    private boolean rolling;

    
    SVGComponent svg;
    


    public Dice() {
        
        left = new Die();
        right = new Die();
        

        svg = new SVGComponent("q.svg");
        
        svg.setVisible(false);


        this.add(left, "pos 0 0 container.h container.h");
        this.add(right, "pos 100%-container.h 0 100% container.h");
        this.right.add(svg, "pos 15% 15% 85% 85%");
    }

    public int rollBoth() {
        rolling = true;
        try {
            Thread.sleep(5);
        } catch (InterruptedException ex) {}
        if (++count == 20) {
                count = 0;
                rolling = false;
                return left.getValue() + right.getValue();
            }
            left.roll();
            right.roll();
            return rollBoth();
       
    }

    private class Die extends CustomComponent {

        private int value;
        private Random random = new Random();

        public Die() {

        }

        public int roll() {
            int val = random.nextInt(6) + 1; // Range 1-6
            setValue(val);
            return val;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int spots) {
            value = spots;
            boolean intrigue = this == right && value == 1;
            svg.setVisible(intrigue);
            if(intrigue) value--;
            paintImmediately(0,0,getWidth(),getHeight()); // Value has changed, must repaint
        }


        @Override
        public void paintContent(Graphics2D g) {
            
            super.paintContent(g);
            
            int w = getWidth();
            int h = getHeight(); // should use to resize spots too.
              int d = w / 5;

            g.setColor(Color.WHITE);
           g.fillRoundRect(0, 0, getWidth(), getHeight(), d , d );

          g.setColor(Color.BLACK);

            if (this != right || value > 1)
                switch (value) {

                    case 1:
                        drawSpot(g, w / 2, h / 2, d);

                        break;

                    case 3:
                    drawSpot(g, w / 2, h / 2, d);

// Fall thru to next case

                case 2:
                    drawSpot(g, w / 4, h / 4, d);

                    drawSpot(g, 3 * w / 4, 3 * h / 4, d);

                    break;

                case 5:
                    drawSpot(g, w / 2, h / 2, d);

// Fall thru to next case

                case 4:

                    drawSpot(g, w / 4, h / 4, d);

                    drawSpot(g, 3 * w / 4, 3 * h / 4, d);

                    drawSpot(g, 3 * w / 4, h / 4, d);

                    drawSpot(g, w / 4, 3 * h / 4, d);

                    break;

                case 6:
                    drawSpot(g, w / 4, h / 4, d);

                    drawSpot(g, 3 * w / 4, 3 * h / 4, d);

                    drawSpot(g, 3 * w / 4, h / 4, d);

                    drawSpot(g, w / 4, 3 * h / 4, d);

                    drawSpot(g, w / 4, h / 2, d);

                    drawSpot(g, 3 * w / 4, h / 2, d);

                    break;

            }
            
            
              if (rolling){
                  g.setColor(Colors.DICE_ROLLING_OVERLAY);
                   g.fillRoundRect(0, 0, getWidth(), getHeight(), d , d );
              }
           

        }

        private void drawSpot(Graphics g, int x, int y, int d) {

            g.fillOval(x - d / 2, y - d / 2, d, d);

        }
    }
}
