package net.fhmp.cludos.components;

import java.awt.Color;
import net.fhmp.Board;
import net.fhmp.Colors;
import net.fhmp.Location;

/**
 *
 * @author Rudi Theunissen
 */
public enum Suspect implements Card{

    SCARLET(Colors.SCARLET, "Kasandra Scarlet"),
    MUSTARD(Colors.MUSTARD, "Jack Mustard"),
    WHITE(  Colors.WHITE,     "Diane White"),
    GREEN(  Colors.GREEN,     "Jacob Green"),
    PEACOCK(Colors.PEACOCK, "Eleanor Peacock"),
    PLUM(   Colors.PLUM,       "Victor Plum");
   
    private final Color color;
    private final String name;
    
    private Location startingLocation;

    private Suspect(Color color, String name) {
        this.color = color;
        this.name = name;
    }
 
    public Color getColor(){
        return color;
    }

    public Location getStartingLocation() {
        return startingLocation;
    }

    public void setStartingLocation(Location startLocation) {
        this.startingLocation = startLocation;
    }
    
    
    
    @Override
    public String toString() {
       return name;
    }

}
