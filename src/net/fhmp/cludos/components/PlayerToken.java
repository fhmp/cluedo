package net.fhmp.cludos.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import net.fhmp.Colors;
import net.fhmp.Fonts;
import net.fhmp.Location;

/**
 *
 * @author Rudi Theunissen
 */
public class PlayerToken extends CustomComponent{
    
    private Suspect suspect;
    private Location location;
    
    public PlayerToken(String playerName, Suspect suspect, Location location){
        this.suspect = suspect;
        setLocation(location);
        
        this.setCustomToolTipText(suspect.toString() + " (" + playerName + ")");
    }
    
    public void paintContent(Graphics2D g) {
        g.setColor(suspect.getColor());
        
        g.fillRoundRect(0,0,getWidth(), getHeight(), getWidth() / 5, getHeight() / 5);
        
        g.setColor(Colors.TOKEN_BORDER);
 
        g.drawRoundRect(0,0,getWidth() - 1, getHeight() - 1,  getWidth() / 5, getHeight() / 5);
        
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, getHeight() / 3));
        
        FontMetrics fm = g.getFontMetrics();
        String n = suspect.toString().substring(0,2);
        
        int x = (getWidth() - fm.stringWidth(n)) / 2;
        int y = Fonts.getBaseline(fm, getHeight());
        
        g.setColor(Color.BLACK);
        
        g.drawString(n, x, y);
        
       
    }
    
    public final void setLocation(Location location) {
        if (this.location != null) {
            this.location.remove(0);
            this.location.repaint();
        }
        this.location = location;
        this.location.add(this, "pos 5% 5% 90% 90%");
        this.location.repaint();
    }

}
