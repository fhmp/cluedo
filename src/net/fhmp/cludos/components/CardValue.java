package net.fhmp.cludos.components;

/**
 *
 * @author Rudi Theunissen
 */
public enum CardValue {
    
    SCARLET,
    PLUM,
    WHITE,
    GREEN,
    MUSTARD,
    PEACOCK

}
