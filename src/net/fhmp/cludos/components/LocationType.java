package net.fhmp.cludos.components;

/**
 *
 * @author Rudi Theunissen
 */
public enum LocationType {
    
  ENTRANCE,
  BLOCKED,
  DOORWAY,
  WALKABLE,
  INTRIGUE,
  STARTING

}
