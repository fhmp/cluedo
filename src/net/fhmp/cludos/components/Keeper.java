
package net.fhmp.cludos.components;

/**
 * A container for the keeper cards.
 * 
 * @author Rudi Theunissen
 */
public enum Keeper implements Card{
    
    // Before roll
    FREE_RANGE("Play instead of rolling the dice. Move anywhere.", 2),
    // 
    CONFIDENTIAL_INFORMANT("Play at the start of your turn. The previous player must show you a card", 1),

    // Before moving
    OVERSTAY("Play when starting a turn in a room. You may stay in that room and start a rumour.", 1),
    // 
    LADDER("Play after you roll the dice but before you move. Add 6 to your dice roll.", 2),
    
    // End of your turn
    SNAKE("Play at the end of your turn. Move anyone back to their start space.", 2),
    // 
    REFRESHER("Play at the end of your turn. Take another turn.", 4),
    
    // When asked to answer a rumour
    PLEAD_THE_FIFTH("Play instead of answering a Rumor. The Rumor stays unanswered.", 2),
   
    // When a card has been shown by and to someone who isn't the cardholder
    SPY("Play when one player has shown another player a card. You get to see the card.", 2);

    private final String description;
    private final int amount;
    
    private Keeper(String descripton, int amount){
        this.description = descripton;
        this.amount = amount;
    }
    
    public int getAmount(){
        return amount;
    }
    
    public String getDescription(){
        return description;
    }
}
