
package net.fhmp.cludos.components;

import net.fhmp.Location;
import java.util.ArrayList;

/**
 *
 * @author Rudi Theunissen
 */
public class Player{
    
   private final ArrayList<Card> hand = new ArrayList<>();
   private PlayerToken token;
   private boolean forcedToMove;
   private int roll;
   private Location location;
   private Suspect suspect;
   private boolean active;
   private String name;
   private boolean abilityEnabled;

    public boolean isAbilityEnabled() {
        return abilityEnabled;
    }

    public void setAbilityEnabled(boolean abilityUnused) {
        this.abilityEnabled = abilityUnused;
    }
   
   
   
   public Player(String name){
       this.active = true;
       this.name = name;
       this.abilityEnabled = true;
   }
   
   

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
   
   

    public int getRoll() {
        return roll;
    }

    public void setRoll(int roll) {
        this.roll = roll;
    }
   
   public String getName(){
       return name;
   }
   
   public Location getLocation(){
       return location;
   }
    
    public ArrayList<Card> getHand(){
        return hand;
    }

    public void setSuspect(Suspect suspect) {
       this.suspect = suspect;
    }
    
    public Suspect getSuspect(){
        return suspect;
    }

    public void setLocation(Location location) {
       this.location = location;
       if(token != null) token.setLocation(location);
    }
    
    public void forceMove(Location location){
        this.location = location;
        forcedToMove = true;
    }
    
    public void setForcedToMove(boolean forcedToMove){
        this.forcedToMove = forcedToMove;
    }

    public boolean wasForcedToMove() {
       return forcedToMove;     
    }
    
    public PlayerToken getToken(){
        return token;
    }

    public void setToken(PlayerToken token) {
        this.token = token;
    }
}
