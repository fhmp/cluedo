package net.fhmp.cludos.components;

import com.kitfox.svg.SVGDiagram;
import com.kitfox.svg.app.beans.SVGIcon;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.net.URL;
import net.fhmp.ResourceLoader;

/**
 *
 * @author Rudi Theunissen
 */
public class SVGImage extends CustomComponent {

    SVGIcon svg;
     SVGDiagram diagram;
     

    public SVGImage() {

//        SVGUniverse svgUniverse = new SVGUniverse();
//   
//
//        URI i;
            URL in = ResourceLoader.getURL("svg.svg");
//            System.out.println(in);
//            i = svgUniverse.loadSVG(in);
//            
            

        svg = new SVGIcon();
        svg.setPreferredSize(new Dimension(200,200));
        
        svg.setScaleToFit(true);
        svg.setAntiAlias(true);



    }

    @Override
    public void paintContent(Graphics2D g) {
       // svg.paintIcon(this, g, 0, 0);
    }
}
