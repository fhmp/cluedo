package net.fhmp.cludos.components;


import java.awt.Dimension;
import net.fhmp.CustomCheckBox;
import net.miginfocom.swing.MigLayout;

public class DetectivePad extends CustomComponent {

    public DetectivePad() {

        super();
        this.setLayout(new MigLayout("debug, ins 0, gap 5 5 0 0, wrap 3", "grow", "grow"));
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 3; j++) {
                add(new CustomCheckBox("Scarlett"), "grow");
            }
        }
    }

}