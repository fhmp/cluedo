/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.fhmp.cludos.components;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JLayeredPane;
import net.fhmp.Colors;
import net.fhmp.Fonts;
import net.fhmp.GraphicsUtils;
import net.fhmp.ResourceLoader;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author PARANOID
 */
public class TabGroup extends JLayeredPane {

    public static final int MAX_TABS = 5;
    private static Area LEFT_SHAPE, RIGHT_SHAPE;
    private MouseListener clickListener;
    private int runningTabWidth;
    private Tab currentTab;
    private ArrayList<Tab> tabs = new ArrayList<Tab>(MAX_TABS);

    public TabGroup() {
        this.setOpaque(false);

        this.setLayout(new MigLayout("insets 0"));


        clickListener = new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                Object src = e.getSource();
                for (int i = 0; i < tabs.size(); i++)
                    if (src == tabs.get(i) && tabs.get(i).getOutline().contains(e.getPoint())) {
                        setCurrentTab(i);
                        return;
                    }
                repaint();
            }
        };
    }

    public void addTab(String title, CustomComponent content) {
        if(tabs.size() == MAX_TABS) return;
        Tab t = new Tab(title, content);
        tabs.add(t);
        t.addMouseListener(clickListener);

        this.add(t, "pos " + (runningTabWidth - (tabs.size() - 1) * 7) + " 0");
        this.setLayer(t, JLayeredPane.DEFAULT_LAYER, tabs.size());
        if (tabs.size() == 1) setCurrentTab(0);
        runningTabWidth += t.getWidth();
    }

    public void setCurrentTab(int index) {
        if (tabs.isEmpty()) return;
        Tab tab = tabs.get(index);
        if (currentTab == tab) return;
        if (currentTab != null) currentTab.setCurrent(false);
        tab.setCurrent(true);
        currentTab = tab;
        this.moveToFront(tab);
        repaint();
        onSwitch(tab);
    //    if(tab.getContent() != null) tab.getContent().onShow();
        
    }

    public void addContent(int tabIndex, Component c, int x, int y) {
        this.tabs.get(tabIndex).getContent().add(c, "pos " + x + " " + (y + 30));
    }

    public int getNumberOfTabs() {
        return tabs.size();
    }

    public void onSwitch(Tab tab) {
    }

    ArrayList<Tab> getTabs() {
        return tabs;
    }

   public int getTabExtent() {
        return runningTabWidth - (tabs.size() - 1) * 7;
    }
    

    public class Tab extends CustomComponent {

        private String title;
        private boolean isCurrent;
        private Area shape;
        private int width;
        private CustomComponent view;
        public static final int MIN_STRING_WIDTH = 60;
        public static final int SIDE_GAP = 15;

        public Tab(String title, CustomComponent view) {
            if (LEFT_SHAPE == null || RIGHT_SHAPE == null) {
                LEFT_SHAPE = GraphicsUtils.imageToArea(GraphicsUtils.cropImage(ResourceLoader.openImage("images/tab.png"), new Rectangle(0, 0, 15, 30)));
                RIGHT_SHAPE = GraphicsUtils.imageToArea(GraphicsUtils.cropImage(ResourceLoader.openImage("images/tab.png"), new Rectangle(45, 0, 15, 30)));

            }

            this.title = title;
            this.view = view;

            this.setOpaque(false);

            int stringWidth = getFontMetrics(Fonts.MEDIUM_BOLD).stringWidth(title) + SIDE_GAP * 2;
         //   if (stringWidth < MIN_STRING_WIDTH) stringWidth = MIN_STRING_WIDTH;

            this.shape = new Area();

            this.shape.add(new Area(new Rectangle(15, 0, stringWidth, 30)));



            AffineTransform shift = new AffineTransform();
            shift.translate(stringWidth + 15, 0);
            Area right = (Area) RIGHT_SHAPE.clone();
            right = new Area(shift.createTransformedShape(right));
            shift = null;


            this.shape.add(LEFT_SHAPE);
            this.shape.add(right);

            width = stringWidth + 30;

            this.setPreferredSize(new Dimension(width, 30));
        }

        public String getTitle() {
            return title;
        }

        public CustomComponent getContent() {
            return view;
        }

        public Area getOutline() {
            return shape;
        }

        @Override
        public int getWidth() {
            return width;
        }

        public boolean isCurrent() {
            return isCurrent;
        }

        public void setCurrent(boolean isCurrent) {
            this.isCurrent = isCurrent;
        }

        @Override
        public void paintContent(Graphics2D g) {
            BufferedImage tabImage = isCurrent ? ResourceLoader.openImage("images/tab_current.png") : ResourceLoader.openImage("images/tab.png");

           g.drawImage(GraphicsUtils.cropImage(tabImage, new Rectangle(0, 0, 15, 30)), 0, 0, null);
           g.drawImage(GraphicsUtils.cropImage(tabImage, new Rectangle(tabImage.getWidth() - 15, 0, 15, 30)), getWidth() - 15, 0, null);
            g.setPaint(new TexturePaint(GraphicsUtils.cropImage(tabImage, new Rectangle(15, 0, 30, 30)), new Rectangle2D.Float(15, 0, 30, 30)));
            g.fillRect(15, 0, getWidth() - 30, 30);
            

            g.setColor(Colors.LIGHT_GRAY);
            g.setFont(Fonts.MEDIUM_BOLD);
            g.drawString(this.title, 15 + SIDE_GAP, 20);
            


        }
    }
}
