package net.fhmp.cludos.components;

import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;
import net.fhmp.CustomTooltip;
import net.fhmp.CustomTooltipComponent;
import net.fhmp.GraphicsUtils;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Rudi Theunissen
 */
public class CustomComponent extends JComponent implements CustomTooltipComponent {

    private CustomTooltip toolTip;

    public CustomComponent() {
        setLayout(new MigLayout("ins 0"));
        this.setFocusable(true);
        this.setRequestFocusEnabled(true);
    }

    public void setCustomTooltip(CustomTooltip toolTip) {
        this.toolTip = toolTip;
    }

    @Override
    public void setCustomToolTipText(String tipText) {
        toolTip = new CustomTooltip(tipText, this);
    }

    @Override
    public CustomTooltip getCustomTooltip() {
        return toolTip;
    }

    public void paintComponent(Graphics graphics) {
        Graphics2D g = (Graphics2D) graphics;
        GraphicsUtils.enableMaximumQuality(g);
        paintContent(g);
    }

    public void paintContent(Graphics2D g) {
        super.paintComponent(g);
    }
}
