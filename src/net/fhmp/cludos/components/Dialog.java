package net.fhmp.cludos.components;

import com.sun.awt.AWTUtilities;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics2D;
import javax.swing.JWindow;
import net.fhmp.Colors;
import net.fhmp.Fonts;

/**
 *
 * @author Rudi Theunissen
 */
public class Dialog extends JWindow {
    
    private final String title;

    public Dialog(String title, CustomComponent content, int width, int height) {
this.title = title;
        this.setAlwaysOnTop(true);
        this.setSize(width + 20, height + 60);
        AWTUtilities.setWindowOpaque(this, false);
        Content container = new Content();
        content.setPreferredSize(new Dimension(width + 20, height+60));
        container.add(content, "pos 10 50, w 100%-20, h 100%-60");
        this.add(container, BorderLayout.CENTER);
        this.setLocationRelativeTo(null);
        setVisible(true);

    }

    private class Content extends CustomComponent {

        public void paintContent(Graphics2D g) {
            g.setColor(Colors.DARK_GRAY);
            g.fillRoundRect(0, 0, getWidth(), getHeight(), 4, 4);
            
            g.setColor(Colors.MEDIUM_GRAY);
            g.fillRoundRect(10, 10, getWidth()-20, 30, 4, 4);
            
            g.setColor(Colors.LIGHT_GRAY);
            g.setFont(Fonts.LARGE);
            
            g.drawString(title, 20, 31);
        }
    }
}
