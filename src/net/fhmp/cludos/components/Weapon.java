package net.fhmp.cludos.components;


/**
 * Represents a Weapon card.
 * 
 * @author PARANOID
 */
public enum Weapon implements Card{
    
    ROPE("Rope"),
    CANDLESTICK("Candlestick"),
    KNIFE("Knife"),
    PISTOL("Pistol"),
    BASEBALL_BAT("Baseball Bat"),
    DUMBBELL("Dumbbell"),
    TROPHY("Trophy"),
    POISON("Poison"),
    AXE("Axe");
    
    private String name;

  private Weapon(String name){
      this.name = name;
  }
  
  public String toString(){
      return name;
  }
}
