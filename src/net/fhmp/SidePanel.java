package net.fhmp;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import net.fhmp.cludos.components.*;

/**
 *
 * @author Rudi Theunissen
 */
public class SidePanel extends CustomComponent {

    private TexturePaint background = new TexturePaint(ResourceLoader.openImage("images/table.jpg"), new Rectangle(0, 0, 600, 600));
    private Dice dice = new Dice();
        JButton cardsButton = new JButton("Show Cards");
    CustomTextField[] nameFields = new CustomTextField[6];
    CustomCheckBox[] aiBoxes = new CustomCheckBox[6];
    JButton startButton = new JButton("Start Game");
    JButton endTurnButton = new JButton("Roll");
    PlayerIndicator pi = new PlayerIndicator();
    private SetupPanel setup = new SetupPanel();
    private GamePanel game = new GamePanel();
    


    private class PlayerIndicator extends CustomComponent {

        private Player player;

        public void setPlayer(Player player) {
            this.player = player;
            repaint();
        }

        public void paintContent(Graphics2D g) {
            if (player == null) return;
            g.setColor(player.getSuspect().getColor());
            g.fillRoundRect(0, 0, getWidth(), getHeight(), 4, 4);
            g.setFont(Fonts.LARGE_BOLD);
            int y = Fonts.getBaseline(g.getFontMetrics(), getHeight());
            int x = (getWidth() - g.getFontMetrics().stringWidth(player.getName())) / 2;
            if (player.getSuspect() == Suspect.WHITE || player.getSuspect() == Suspect.MUSTARD)
                g.setColor(Colors.DARK_GRAY);
            else g.setColor(Colors.LIGHT_GRAY);

            g.drawString(player.getName(), x, y);
        }
    }
    
    private class GamePanel extends CustomComponent {

        public GamePanel() {
            this.add(endTurnButton, "pos container.w/2+5 pi.y2+10 container.w pi.y2+10+pi.h");
            this.add(dice, "id dice, pos 0 0 container.w/2-5 (container.w/2-5)/2-5");
            this.add(pi, "id pi, pos container.w/2+5 0 container.w ((container.w/2-5)/2-5)/2-5");
            this.add(cardsButton, "pos dice.x dice.y2+10, w 50%-20, h 50%-20");
            this.setBorder(new LineBorder(Color.RED));
        }
    }

    public SidePanel() {


        this.add(setup, "pos 10 10 100%-10 50%-10");
        this.add(game, "pos 10 10 100%-10 50%-10");
       

        startButton.setEnabled(false);

        endTurnButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (endTurnButton.getText().equals("End Turn")) {
                    endTurnButton.setText("Roll");
                    Cluedo.CONTROLLER.endTurn();
                } else {
                    endTurnButton.setText("End Turn");
                    MainFrame.boardPanel.clearHighlights();
                    MainFrame.boardPanel.highlightReachableLocations(Cluedo.CONTROLLER.getCurrentPlayer().getLocation(), dice.rollBoth());
                }
            }
        });

        reset();

    }

    public void reset() {
        game.setVisible(false);
        setup.setVisible(true);
        setup.reset();
    }

    public void start() {
        game.setVisible(true);
        setup.setVisible(false);

        ArrayList<Player> players = new ArrayList<>();

        for (int i = 0; i < nameFields.length; i++) {
            if (nameFields[i].getText().length() == 0) continue;
            Player player = new Player(nameFields[i].getText());
            Suspect suspect = Suspect.values()[i];
            player.setSuspect(suspect);
            player.setLocation(suspect.getStartingLocation());
            player.setToken(new PlayerToken(nameFields[i].getText(), suspect, suspect.getStartingLocation()));
            players.add(player);
        }



        Cluedo.CONTROLLER.setPlayers(players);
        Cluedo.CONTROLLER.start();


    }

    public void setIndicatorPlayer(Player player) {
        pi.setPlayer(player);
    }

    @Override
    public void paintContent(Graphics2D g) {
        //   g.setPaint(background);
        g.setColor(Colors.DARK_GRAY);
        g.fillRect(0, 0, getWidth(), getHeight());

    }

    private class SetupPanel extends CustomComponent {

        public void paintContent(Graphics2D g) {
            g.setColor(Colors.LIGHT_GRAY);
            g.setFont(Fonts.LARGE_BOLD);

            g.drawString("Player Setup", 10, 25);


            g.setFont(Fonts.MEDIUM_BOLD);
            g.drawString("AI", getWidth() - getWidth() / 5 + 20, 30);

        }

        public SetupPanel() {

            startButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    start();
                }
            });


            for (int i = 0; i < aiBoxes.length; i++) {
                aiBoxes[i] = new CustomCheckBox();
                aiBoxes[i].setCustomToolTipText("AI player?");
                this.add(aiBoxes[i], "pos 80%+20 " + (i * 40 + 44) + ", w 80%");
            }







            for (int i = 0; i < nameFields.length; i++) {
                nameFields[i] = new CustomTextField("Player Name");


                nameFields[i].addCaretListener(new CaretListener() {

                    @Override
                    public void caretUpdate(CaretEvent e) {
                        int count = 0;
                        for (int i = 0; i < nameFields.length; i++) {
                            if (nameFields[i].getText().length() == 0) continue;
                            count++;
                        }
                        startButton.setEnabled((count >= 3));
                    }
                });

                Suspect suspect = Suspect.values()[i];

                BufferedImage img = new BufferedImage(30, 30, BufferedImage.TYPE_INT_ARGB);
                Graphics2D g = img.createGraphics();
                GraphicsUtils.enableMaximumQuality(g);

                g.setColor(Colors.VERY_DARK_GRAY);
                g.fillRoundRect(0, 0, 30, 30, 4, 4);

                g.setColor(suspect.getColor());
                g.fillRoundRect(1, 1, 28, 28, 4, 4);

                g.dispose();
                nameFields[i].setIcon(img);

                nameFields[i].getIcon().setCustomToolTipText(suspect.toString());
                this.add(nameFields[i], "pos 10 " + (i * 40 + 40) + ", w 80%");
            }

            this.add(startButton, "pos 10 500, w 80%, h 30");

        }

        public void reset() {
            for (CustomCheckBox c : aiBoxes) {
                c.setSelected(false);
            }

            for (CustomTextField t : nameFields) {
                t.setText("");
            }

            nameFields[0].requestFocusInWindow();
        }
    }
}
