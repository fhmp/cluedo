package net.fhmp;

import java.awt.Color;

/**
 *
 * @author Rudi Theunissen
 */
public class Colors {

    public static final Color DARK_GRAY = Color.decode("#444444");
    public static final Color LIGHT_GRAY = Color.decode("#eeeeee");
    public static final Color DICE_ROLLING_OVERLAY = new Color(255,255,255,200);
    
        public static final Color MEDIUM_GRAY = new Color(136, 136, 136);
    public static final Color DARK_GRAY_LIGHTER = new Color(76, 76, 76);
    public static final Color DARK_GRAY_DARKER = new Color(60, 60, 60);
     public static final Color VERY_DARK_GRAY = new Color(50, 50,50);
    public static final Color LIGHT_GRAY_DARKER = new Color(214, 214, 214);
    public static final Color LIGHT_GRAY_LIGHTER = new Color(255, 255, 255);
      public static final Color TOOLTIP_BORDER = new Color(68,68,68,125);
    
    
    public static final Color SCARLET = Color.decode("#c0322e");
    public static final Color PEACOCK = Color.decode("#334b7b");
    public static final Color GREEN = Color.decode("#337b3b");
    public static final Color PLUM = Color.decode("#5e347c");
    public static final Color WHITE = Color.WHITE;
    public static final Color MUSTARD = Color.decode("#ef9c1a");
    public static final Color TOKEN_BORDER = new Color(0,0,0,100);
    
    public static final Color [] SUSPECT_COLORS = {SCARLET, PEACOCK, GREEN, PLUM, WHITE, MUSTARD};
    public static final Color LOCATION_HIGHLIGHT = new Color(20,20,90,100);
    public static final Color LOCATION_HIGHLIGHT_ROLLOVER = new Color(20,90,90,100);
}
