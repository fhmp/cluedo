package net.fhmp;

import java.awt.BorderLayout;
import java.awt.Container;
import java.util.HashMap;
import javax.swing.SwingUtilities;

/**
 * Used to switch views within a JFrame.
 *
 * @param <C>
 * @author FHMP
 */
public class ViewSwitcher<C extends Container> {

    /**
     * Map to keep track of views according to their specified names
     */
    private HashMap<String, C> views = new HashMap<>();
    /**
     * The host container that contains the views to be switched between
     */
    private Container host;
    /**
     * Used to keep track of the current view
     */
    private C current;

    public ViewSwitcher() {
    }

    public ViewSwitcher(Container host) {
        this.host = host;
    }

    /**
     * Registers a view bound to a specified name
     *
     * @param key
     * @param view the view
     */
    public void registerView(C view, Object key) {
        views.put(key.toString(), view);
    }

    /**
     * Sets the host container that will contain the view
     *
     * @param host the host container
     */
    public void setHost(Container host) {
        this.host = host;
    }

    /**      *
     * Switches to the view bound to the specified name
     *
     * @param key the key of the view to switch to
     */
    public void switchTo(final Object key) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                C view = views.get(key.toString());

                System.out.println(view);
                if (current
                        != null) {
                    host.remove(current);
                }
                current = view;

                host.add(view,
                        "pos 0 0 100% 100%");
                host.validate();

                host.repaint(); // just to make sure  
            }
        });

    }
}
