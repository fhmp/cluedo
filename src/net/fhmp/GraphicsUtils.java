/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.fhmp;

import java.awt.*;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;

/**
 *
 * @author Paranoid
 */
public class GraphicsUtils {
    
        public static TexturePaint TEXTFIELD_MIDDLE;
    public static BufferedImage TEXTFIELD_LEFT, TEXTFIELD_RIGHT;
    
    public static final TexturePaint NOISE = new TexturePaint(ResourceLoader.openImage("images/noise.png"), new Rectangle(0,0,100,100));
    public static void enableMaximumQuality(Graphics2D g) {
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
       g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    }
    
        public static BufferedImage cropImage(BufferedImage src, Rectangle rect) {
        return src.getSubimage(rect.x, rect.y, rect.width, rect.height);
    }
        
            public static void paintTextFieldOverlay(Graphics2D g, int w) {
        if (TEXTFIELD_MIDDLE == null || TEXTFIELD_RIGHT == null || TEXTFIELD_MIDDLE == null) {
            BufferedImage overlay = ResourceLoader.openImage("images/components/textfield_overlay.png");
            TEXTFIELD_MIDDLE = new TexturePaint(GraphicsUtils.cropImage(overlay, new Rectangle(3, 0, 1, 30)), new Rectangle(3, 0, 1, 30));
            TEXTFIELD_LEFT = GraphicsUtils.cropImage(overlay, new Rectangle(0, 0, 3, 30));
            TEXTFIELD_RIGHT = GraphicsUtils.cropImage(overlay, new Rectangle(4, 0, 3, 30));
        }

        g.drawImage(TEXTFIELD_LEFT, 0, 0, null);
        g.setPaint(TEXTFIELD_MIDDLE);
        g.fillRect(3, 0, w - 6, 30);
        g.drawImage(TEXTFIELD_RIGHT, w - 3, 0, null);
    }
    
     public static Area imageToArea(BufferedImage image) {
        Area a = new Area();
        Rectangle r = new Rectangle();
        r.setSize(1, 1);
        for (int x = 0; x < image.getWidth(); x++)
            for (int y = 0; y < image.getHeight(); y++)
                if (image.getRGB(x, y) >>> 24 > 0) {
                    r.setLocation(x, y);
                    a.add(new Area(r));
                }
        return a;
    }
}
