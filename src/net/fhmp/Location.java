package net.fhmp;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import net.fhmp.cludos.components.CustomComponent;
import net.fhmp.cludos.components.LocationType;
import net.fhmp.cludos.components.Room;

public class Location extends CustomComponent{

    private LocationType locationType = LocationType.BLOCKED;
    private boolean rollover;
    private boolean highlighted;
    private Room entrance;
    
    public void setHighlighted(boolean highlighted){
        this.highlighted = highlighted;
        if(highlighted) setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        else setCursor(Cursor.getDefaultCursor());
    }
    
    private static MouseListener listener = new Listener();

    private LinkedList<Location> neighbours;
    
    public final int row, col;

    public Location(int row, int col){
        this.row = row;
        this.col = col;
        
        this.addMouseListener(listener);
    }
    
    @Override
    public void paintContent(Graphics2D g){
        
        if(highlighted) paintHighlight(g);
 
     //   g.drawString(row+":"+col, 0, getHeight() / 2);
    }
    
    private void paintHighlight(Graphics2D g){
        if (rollover) g.setColor(Colors.LOCATION_HIGHLIGHT_ROLLOVER);
        else g.setColor(Colors.LOCATION_HIGHLIGHT);
        g.fillRect(0, 0, getWidth(), getHeight());
    }
    
    public void addNeighbour(Location location){
        if(neighbours == null) neighbours = new LinkedList<>();
        neighbours.add(location);
    }
    
    public LinkedList<Location> getNeighbours(){
        return neighbours;
    }
    
    public boolean isIntrigue(){
         return locationType == LocationType.INTRIGUE;
    }

    public Room getEntrance() {
        return entrance;
    }

    public void setEntrance(Room entrance) {
        this.entrance = entrance;
    }
    
    @Override
    public String toString(){
        return row + ":" + col;
    }

    @Override
    public int hashCode() {
        return row * 31 + col;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Location)) return false;
        final Location other = (Location) obj;
        if (this.row != other.row) return false;
        if (this.col != other.col) return false;
        return true;
    }

    public void setLocationType(LocationType locationType) {
        this.locationType = locationType;
    }

    public LocationType getLocationType() {
        return locationType;
    }
    
    private void onClick(){
        if(!highlighted) return;
        // move player towards clicked location
        Cluedo.CONTROLLER.movePlayer(this);
    } 
    
    public boolean isWalkable(){
        return locationType == LocationType.WALKABLE;
    }
    
    public boolean isDoorway(){
        return locationType == LocationType.DOORWAY;
    }
    
    public boolean isEntrance(){
         return locationType == LocationType.ENTRANCE;
    }

    boolean isBlocked() {
        return locationType == LocationType.BLOCKED;
    }

    private static class Listener extends MouseAdapter {

        public void mouseEntered(MouseEvent e) {
            Location src = (Location) e. getSource();
            src.rollover = true;
            src.repaint();
        }

        public void mouseExited(MouseEvent e) {
            Location src = (Location) e. getSource();
            src.rollover = false;
            src.repaint();
        }
        
        public void mouseReleased(MouseEvent e){
            Location src = (Location) e. getSource();
            if(!src.contains(e.getPoint())) return;
            src.onClick();
        }
    }
}
