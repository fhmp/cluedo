package net.fhmp;

import com.kitfox.svg.app.beans.SVGIcon;
import java.awt.Graphics2D;
import net.fhmp.cludos.components.CustomComponent;

/**
 *
 * @author Rudi Theunissen
 */
public class SVGComponent extends CustomComponent {

    private SVGIcon svg = new SVGIcon() {

        @Override
        public int getIconWidth() {
            return getWidth();
        }

        @Override
        public int getIconHeight() {
            return getHeight();
        }
    };

    public SVGComponent(String location) {
        svg.setSvgURI(ResourceLoader.getURI(location));
        svg.setScaleToFit(true);
        svg.setAntiAlias(true);
    }

    @Override
    public void paintContent(Graphics2D g) {
        svg.paintIcon(this, g, 0, 0);
    }
}
