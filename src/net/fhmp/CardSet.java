package net.fhmp;

import java.util.*;
import net.fhmp.cludos.components.*;

/**
 * Manages the card representations in the game
 * 
 * @author Rudi Theunissen
 */
public class CardSet {

    public static final int CLOCK_AMOUNT = 8;
    
    
    
    private ArrayList<Card> intrigueCards = new ArrayList<>();
    private ArrayList<Card> rumourCards = new ArrayList<>();
    private LinkedList<Card> stash;
  
    private Card[] solution = new Card[3];

    public CardSet() {

        rumourCards.addAll(Arrays.asList(Suspect.values()));
        rumourCards.addAll(Arrays.asList(Room.values()));
        rumourCards.addAll(Arrays.asList(Weapon.values()));

        // not a valid rumour card
        rumourCards.remove(Room.SWIMMING_POOL);

        for (int i = 0; i < CLOCK_AMOUNT; i++)
            intrigueCards.add(new Clock());

        for (Keeper keeper : Keeper.values())
            for (int j = 0; j < keeper.getAmount(); j++)
                intrigueCards.add(keeper);
    }

    public final void shuffle() {
        Collections.shuffle(intrigueCards);
        Collections.shuffle(rumourCards);
    }

    private void createSolution() {
        shuffle();
        Arrays.fill(solution, null);
        int count = 0;
        Iterator<Card> i = rumourCards.iterator();
        while(i.hasNext() && count < 3) {
            Card card = i.next();
            int index = getIndex(card);
            if(solution[index] == null){
                solution[index] = card;
                count++;
                i.remove();
            }
        }
    }
    
    public boolean verifyAccusation(Suspect suspect, Weapon weapon, Room room) {
        return solution[getIndex(suspect)] == suspect
                && solution[getIndex(weapon)] == weapon
                && solution[getIndex(room)] == room;
    }
        
       private int getIndex(Card card){
           if(card instanceof Suspect) return 0;
           if(card instanceof Weapon) return 1;
           return 2;
       }

    public void deal(List<Player> players) {
        createSolution();

        Iterator<Card> rumourDealer = rumourCards.iterator();
        for (Player player : players) {
            for (int i = 0; i < rumourCards.size() / players.size(); i++) {
                player.getHand().add(rumourDealer.next());
            }
        }

        // place the leftover cards in the left-over part of the pool  
        if (rumourDealer.hasNext()) {
            stash = new LinkedList<>();
            while (rumourDealer.hasNext()) {
                stash.add(rumourDealer.next());
            }
        }
    }

    public List<Card> getRumorCards() {
        return rumourCards;
    }

    public List<Card> getIntrigueCards() {
        return intrigueCards;
    }

    public Card[] getSolution() {
        return solution;
    }

    public List<Card> getStash() {
        return stash;
    }
}
