/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.fhmp;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Paranoid
 */
public class Fonts {
   
    public static final Font SMALLEST = Fonts.getCustomFont("mplus-1c-medium.ttf", 11);
    public static final Font SMALL = Fonts.getCustomFont("mplus-1c-medium.ttf", 13);
    public static final Font MEDIUM = Fonts.getCustomFont("mplus-1c-medium.ttf", 13);
    public static final Font LARGE = Fonts.getCustomFont("mplus-1c-medium.ttf", 16);
    public static final Font HEAVY = Fonts.getCustomFont("mplus-1c-medium.ttf", 24);
    public static final Font EXTRA = Fonts.getCustomFont("mplus-1c-medium.ttf", 32);
    
    public static final Font PARAGRAPH = Fonts.getCustomFont("mplus-1c-bold.ttf", 13);
    public static final Font MEDIUM_BOLD = Fonts.getCustomFont("mplus-1c-bold.ttf", 13);
    public static final Font LARGE_BOLD = Fonts.getCustomFont("mplus-1c-bold.ttf", 16);
    
    public static final Font LABEL = Fonts.getCustomFont("mplus-1c-light.ttf", 28);
    public static final Font ALT = Fonts.getCustomFont("mplus-1c-light.ttf", 42);
    public static final Font SUPER = Fonts.getCustomFont("mplus-1c-light.ttf", 42);

    private Fonts() {
    }

    public static int getBaseline(FontMetrics fm, int height) {
        int b = fm.getHeight();
        return (height + b) / 2 - fm.getDescent();
    }


   /**
    * Returns a Font object, using a custom TrueType font file.
    * @param name filename, including relative location to main.

     * @param size font size
     * @return Font object
     */
    public static Font getCustomFont(String name, float size) {
        Font font = null;
        try {
            try (InputStream in = ResourceLoader.openStream("fonts/" + name)) {
             font = Font.createFont(Font.TRUETYPE_FONT, in);
            }
        } catch (FontFormatException | IOException ex) {
            font = new Font(Font.SANS_SERIF, Font.PLAIN, 0);
        }
        return font.deriveFont(size);
    }
}
