/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.fhmp;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.*;
import net.fhmp.cludos.components.CustomComponent;


public class CustomCheckBox extends CustomComponent {

    public boolean isSelected;
    public boolean isPressed;
    public String label;

    public CustomCheckBox(String label) {
        this();
        this.label = label;
        this.setFont(Fonts.MEDIUM);
        this.setPreferredSize(new Dimension(getFontMetrics(getFont()).stringWidth(label) + 34, 24));
    }

    public void setPreferredSize(Dimension d) {
        super.setPreferredSize(new Dimension(d.width, d.height + 1));
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
        repaint();
    }


    public CustomCheckBox() {
        super();
        this.setPreferredSize(new Dimension(24, 24));
        
        this.addKeyListener(new KeyAdapter(){
            public void keyPressed(KeyEvent e){
                if(e.getKeyCode() == KeyEvent.VK_SPACE){
                    toggle();
                }
            }
        });
        
        this.addFocusListener(new FocusAdapter(){
            public void focusGained(FocusEvent e){
                repaint();
            }
            
            public void focusLost(FocusEvent e){
                repaint();
            }
        });

       this.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(MouseEvent e) {
                CustomCheckBox button = (CustomCheckBox) e.getSource();
                if (button.contains(e.getPoint()))
                    toggle();
            }
        });
    }

    public void toggle() {
        isSelected = !isSelected;
        if (!isSelected) {
            fireDeselect();
        } else {
            fireSelect();
        }
       paintImmediately(0, 0, getWidth(), getHeight());
    }

    public void fireSelect() {
    }

    public void fireDeselect() {
    }

    @Override
    public void paintContent(Graphics2D g) {
        if (label != null) {
            g.setColor(Colors.LIGHT_GRAY);
            g.drawString(label, 34, 17);
        }
        g.drawImage(ResourceLoader.openImage("images/checkbox/checkbox_base.png"), 0, 0, null);
        
        if(this.isFocusOwner()){
            g.setColor(Colors.DARK_GRAY_LIGHTER);
            g.fillRoundRect(1,1,22,22, 4, 4);
        }

        if (isSelected) {
            g.drawImage(ResourceLoader.openImage("images/checkbox/check.png"), 0, 1, null);
        }
    }
}