/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.fhmp;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CaretListener;
import net.fhmp.cludos.components.CustomComponent;
import net.miginfocom.swing.MigLayout;
import sun.swing.SwingUtilities2;


/**
 *
 * @author Paranoid
 */
public class CustomTextField extends CustomComponent {

    private AbstractTextField field;
    private CustomComponent icon;
    
    public CustomComponent getIcon(){
        return icon;
    }
        
    public void setIcon(final BufferedImage icon) {
        this.icon = new CustomComponent(){
            public void paintContent(Graphics2D g){
                g.drawImage(icon, 0, 0, null);
            }
        };
                this.icon.setFocusable(false);
        ((MigLayout) this.getLayout()).setComponentConstraints(field, "pos 40 0, width 100%-40, height 100%");
        this.add(this.icon, "pos 5 0 35 30");
    }
    
    public CustomTextField(String idleText) {
        this();
        field.setIdleText(idleText);
    }

    public CustomTextField() {
        field = new AbstractTextField();
        this.add(field, "pos 0 0, w 100%, h 100%");
        this.setFocusable(false);
    }
    
    public boolean validInput(){
        if(field.getText().length() == 0) return false;
        return true;
    }

    public JTextField getField() {
        return field;
    }

    @Override
    public boolean requestFocusInWindow() {
        return this.field.requestFocusInWindow();
    }

    @Override
    public void requestFocus() {
        this.field.requestFocus();
    }
    
    @Override
    public int getHeight(){
        return 30;
    }

    @Override
    public void grabFocus() {
        this.field.grabFocus();
    }

    private class AbstractTextField extends JTextField {

        private String idleText = "";

        public AbstractTextField() {
            super();

            setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
             
             
            this.putClientProperty(SwingUtilities2.AA_TEXT_PROPERTY_KEY, null);
            this.setOpaque(false);
            this.setFocusable(true);
            this.setRequestFocusEnabled(true);
            this.setFont(Fonts.MEDIUM);
            this.setBorder(new EmptyBorder(3, 7, 0, 7));
            this.setCaretColor(Colors.MEDIUM_GRAY);
            this.setForeground(Colors.LIGHT_GRAY);
            this.setSelectionColor(Colors.MEDIUM_GRAY);
            this.setSelectedTextColor(Colors.LIGHT_GRAY);
            this.setLayout(new MigLayout("ins 0"));
            
//            this.addFocusListener(new java.awt.event.FocusListener() {
//
//                @Override
//                public void focusGained(FocusEvent evt) {
//                 //   selectAll();
//                    //      indicator.setState(CustomTextField.INDICATOR_BLANK);
//                }
//
//                @Override
//                public void focusLost(FocusEvent e) {
////                    if (formatter != null) {
////                        String text = getText().replaceAll("[^0-9]", "");
////                        if (text.length() == 0) return;
////                        text = formatter.format(Integer.parseInt(text));
////                        setText(text);
////                    }
//  
//                }
//            });
        }


     //   public void setIndicatorvisible(boolean visible) {
      //      if (indicator != null) this.indicator.setVisible(visible);
    //    }

        public void setCentered(boolean centered) {
            if (centered) this.setHorizontalAlignment(JTextField.CENTER);
            else this.setHorizontalAlignment(JTextField.LEFT);
        }

        public AbstractTextField(String idleText) {
            this();
            this.idleText = idleText;
        }

        public void setIdleText(String idleText) {
            this.idleText = idleText;
        }

        @Override
        public void paintComponent(Graphics paramGraphics) {
            Graphics2D g = (Graphics2D) paramGraphics;
            GraphicsUtils.enableMaximumQuality(g);
            g.setColor(Colors.VERY_DARK_GRAY);
            g.fillRoundRect(0, 0, getWidth(), getHeight(), 4, 4);
            super.paintComponent(g);

            if (getText().length() == 0) {
                g.setColor(Colors.MEDIUM_GRAY);
                g.drawString(idleText, this.getHorizontalAlignment() == JTextField.CENTER ? getWidth() / 2 - g.getFontMetrics().stringWidth(idleText) / 2 : 7, 20);
            }
            GraphicsUtils.paintTextFieldOverlay(g, getWidth());
        }
    }

    public void setText(String text) {
        this.field.setText(text);
    }

    @Override
    public void setBorder(Border border) {
        this.field.setBorder(border);
    }

    public String getText() {
        return this.field.getText();
    }

    public void addCaretListener(CaretListener listener) {
        this.field.addCaretListener(listener);
    }

    public void addActionListener(ActionListener listener) {
        this.field.addActionListener(listener);
    }
    public void setEditable(boolean editable) {
        this.field.setEditable(editable);
    }

    public void setCentered(boolean centered) {
        this.field.setCentered(centered);
    }

    public void setIdleText(String idleText) {
        this.field.setIdleText(idleText);
    }
}
